#!/bin/bash

echo "Scandir ist gestartet..."
quellpfad="/source_dir"
zielpfad="/target_dir"
n=1
while [ $n -le 1 ]
do
  java scandir $quellpfad $zielpfad
  result=$?
  if [ $result -ne 1 ] 
  then
    n=$(( n+1 )) # Abbruchbedingung
  else
    cp $(< scandir.file)
  fi  
  # n=$(( n+1 ))	 # Einmal hochzählen für test; Auskommentieren für Endlosschleife...
done
echo "Scandir ist beendet..."  
