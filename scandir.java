import java.io.*;
import java.text.*;
import java.util.Calendar;
import java.nio.file.*;

class scandir
{
  private static SimpleDateFormat sdf;
  private static Calendar cal;

  private static void writefile(String strscanfile, String str) 
    throws IOException {
      FileOutputStream outputStream = new FileOutputStream(strscanfile);
      byte[] strToBytes = str.getBytes();
      outputStream.write(strToBytes);
 
      outputStream.close();
  }

  private static String usingBufferedReader(String filePath)
  {
    StringBuilder contentBuilder = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new FileReader(filePath)))
    {
 
        String sCurrentLine;
        while ((sCurrentLine = br.readLine()) != null)
        {
            contentBuilder.append(sCurrentLine).append("\n");
        }
    }
    catch (IOException e)
    {
        e.printStackTrace();
    }
    return contentBuilder.toString();
  }  

  private static void scanverz(String strquellpfad, String strzielpfad)  throws IOException {
  String aktzeit;	
  String modzeit;
  String strquell;
  String strziel;
  String strfile;
  String strscantime;
  boolean boolweiter;
  
    strscantime=strquellpfad+"/scandir.time";
    File scanFile = new File(strscantime);
 	 if (scanFile.exists()) {
      aktzeit=usingBufferedReader(scanFile.getAbsolutePath());
    } else {
      cal = Calendar.getInstance();
	   aktzeit=sdf.format(cal.getTime());
	 }
    System.out.println("Scanzeitdatei:"+scanFile.getAbsolutePath());
    System.out.println("Scanzeitpunkt:"+aktzeit);
    boolweiter=true;
    File dir = new File(strquellpfad);
    do {
      File[] filesList = dir.listFiles();
      if (filesList != null) {
        for (File file : filesList) {
          if (file.isFile()) {
            modzeit=sdf.format(file.lastModified());
	         if (modzeit.compareTo(aktzeit)>0) {
	       	  int len = file.getName().length();
	       	  if (file.getName().substring(len-3).equals("jpg")) {
	             System.out.println("Datei:"+file.getName()+" "+sdf.format(file.lastModified()));
                strfile=strquellpfad+"/"+file.getName()+" "+strzielpfad;
	             writefile("scandir.file",strfile);
                cal = Calendar.getInstance();
                aktzeit=sdf.format(cal.getTime());
                writefile(strscantime,aktzeit);
 	             boolweiter=false;
	           }  
	         }
	       }      
        }
      }
    } while (boolweiter);  
  }
	
  public static void main(String[] args) throws IOException
  {
  	 String strquellpfad;
  	 String strzielpfad;
  	 sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    if (args.length > 0) { 
      strquellpfad=args[0];
	 } else {
	   strquellpfad="";
	 }
    if (args.length > 1) { 
      strzielpfad=args[1];
	 } else {
	   strzielpfad="";
	 }
	 scanverz(strquellpfad,strzielpfad);
	 System.exit(1);
  }
  
}  