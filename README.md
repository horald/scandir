# scandir

Scans a directory and writes the found file name into a text file using the timestamp.

You have to compile it:<br>
javac scandir.java

then you can start it:<br>
java scandir /source_dir /target_dir
